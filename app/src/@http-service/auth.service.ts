import { Injectable } from '@angular/core';
import { APICONFIG } from './config/api-config';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';
@Injectable()
export class AuthService {

    constructor(
        private http: HttpClient
    ) { }
    public Login(body: any) {
        
        
        return this.http.post(`${APICONFIG.BASEPOINT}${APICONFIG.AUTH.LOGIN}`, body).toPromise()
         .then(data => {
            localStorage.setItem('data', JSON.stringify(data));
             return data;
             // this.toastr.success('Thêm tài khoản thành công');
         })
         .catch(err => {
             return err;
         //   this.toastr.error(err.error.message);
         });
       }
}